<?php
/**
 * This class allows to define default permission subject class.
 * Can be consider is base of all permission subject types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\subject\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\permission\subject\api\PermissionSubjectInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\permission\subject\library\ConstPermissionSubject;
use liberty_code\role\permission\subject\exception\PermissionCollectionInvalidFormatException;



class DefaultPermissionSubject extends FixBean implements PermissionSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionCollectionInterface $objPermissionCollection = null
     */
    public function __construct(PermissionCollectionInterface $objPermissionCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init permission collection, if required
        if(!is_null($objPermissionCollection))
        {
            $this->setPermissionCollection($objPermissionCollection);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION))
        {
            $this->__beanTabData[ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION:
                    PermissionCollectionInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Return result
        return $this->getObjPermissionCollection()->checkPermissionEnable($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjPermissionCollection()
    {
        // Return result
        return $this->beanGet(ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setPermissionCollection(PermissionCollectionInterface $objPermissionCollection)
    {
        $this->beanSet(ConstPermissionSubject::DATA_KEY_DEFAULT_PERMISSION_COLLECTION, $objPermissionCollection);
    }



}