<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\subject\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\role\permission\build\api\BuilderInterface;
use liberty_code\role\permission\subject\api\PermissionSubjectInterface;



class ToolBoxPermissionBuilder extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate permission collections,
     * from specified subject|array of subjects,
     * with specified permission builder|array of permission builders.
     *
     * @param PermissionSubjectInterface|PermissionSubjectInterface[] $subject
     * @param BuilderInterface|BuilderInterface[] $permissionBuilder
     * @param boolean $boolClear = true
     */
    public static function hydratePermissionCollection(
        $subject,
        $permissionBuilder,
        $boolClear = true
    )
    {
        // Init var
        $tabSubject = (is_array($subject)? array_values($subject) : array($subject));
        $tabPermissionBuilder = (is_array($permissionBuilder)? array_values($permissionBuilder) : array($permissionBuilder));
        $boolClear = (is_bool($boolClear) ? $boolClear : true);

        // Run each subject
        foreach($tabSubject as $objSubject)
        {
            if($objSubject instanceof PermissionSubjectInterface)
            {
                // Run each permission builder
                foreach($tabPermissionBuilder as $objPermissionBuilder)
                {
                    if($objPermissionBuilder instanceof BuilderInterface)
                    {
                        // Hydrate subject permission collection
                        $objPermissionBuilder->hydratePermissionCollection(
                            $objSubject->getObjPermissionCollection(),
                            $boolClear
                        );
                    }
                }
            }
        }
    }



}