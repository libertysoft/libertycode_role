<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\subject\library;



class ConstPermissionSubject
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PERMISSION_COLLECTION = 'objPermissionCollection';



    // Exception message constants
    const EXCEPT_MSG_PERMISSION_COLLECTION_INVALID_FORMAT =
        'Following permission collection "%1$s" invalid! It must be a permission collection object.';
}