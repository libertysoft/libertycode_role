<?php
/**
 * Description :
 * This class allows to describe behavior of permission subject class.
 * Permission subject is subject, where permissions can be attributed.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\subject\api;

use liberty_code\role\subject\api\SubjectInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;



interface PermissionSubjectInterface extends SubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get permission collection object.
     *
     * @return PermissionCollectionInterface
     */
    public function getObjPermissionCollection();





    // Methods setters
    // ******************************************************************************

    /**
     * Set permission collection object.
     *
     * @param PermissionCollectionInterface $objPermissionCollection
     */
    public function setPermissionCollection(PermissionCollectionInterface $objPermissionCollection);
}