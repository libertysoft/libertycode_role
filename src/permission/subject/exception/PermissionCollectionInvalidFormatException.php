<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\subject\exception;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\permission\subject\library\ConstPermissionSubject;



class PermissionCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $permissionCollection
     */
	public function __construct($permissionCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPermissionSubject::EXCEPT_MSG_PERMISSION_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($permissionCollection), 0, 50, "...")
        );
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified permission collection has valid format.
     *
     * @param mixed $permissionCollection
     * @return boolean
     * @throws static
     */
    static public function setCheck($permissionCollection)
    {
        // Init var
        $result = (
            (is_null($permissionCollection)) ||
            ($permissionCollection instanceof PermissionCollectionInterface)
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($permissionCollection);
        }

        // Return result
        return $result;
    }
	
	
	
}