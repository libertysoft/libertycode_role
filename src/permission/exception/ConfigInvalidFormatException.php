<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\exception;

use liberty_code\role\permission\library\ConstPermission;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPermission::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            isset($config[ConstPermission::TAB_CONFIG_KEY_KEY]) &&
            is_string($config[ConstPermission::TAB_CONFIG_KEY_KEY]) &&
            (trim($config[ConstPermission::TAB_CONFIG_KEY_KEY]) != '') &&

            // Check valid enable
            (
                (!isset($config[ConstPermission::TAB_CONFIG_KEY_ENABLE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstPermission::TAB_CONFIG_KEY_ENABLE]) ||
                    is_int($config[ConstPermission::TAB_CONFIG_KEY_ENABLE]) ||
                    (
                        is_string($config[ConstPermission::TAB_CONFIG_KEY_ENABLE]) &&
                        ctype_digit($config[ConstPermission::TAB_CONFIG_KEY_ENABLE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}