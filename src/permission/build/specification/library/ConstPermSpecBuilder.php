<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\build\specification\library;



class ConstPermSpecBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Properties
    const DATA_KEY_DEFAULT_PERM_SPEC = 'objPermSpec';


	
	// Exception message
    const EXCEPT_MSG_PERM_SPEC_INVALID_FORMAT =
        'Following permission specification "%1$s" invalid! It must be null or a valid permission specification object.';
}