<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\build\specification\exception;

use liberty_code\role\permission\specification\api\PermSpecInterface;
use liberty_code\role\permission\build\specification\library\ConstPermSpecBuilder;



class PermSpecInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $permSpec
     */
	public function __construct($permSpec)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPermSpecBuilder::EXCEPT_MSG_PERM_SPEC_INVALID_FORMAT,
            mb_strimwidth(strval($permSpec), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified permission specification has valid format.
	 * 
     * @param mixed $permSpec
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($permSpec)
    {
		// Init var
		$result = (
			(is_null($permSpec)) ||
			($permSpec instanceof PermSpecInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($permSpec);
		}
		
		// Return result
		return $result;
    }
	
	
	
}