<?php
/**
 * Description :
 * This class allows to define permission specification builder class.
 * Permission specification builder allows to populate permission collection,
 * from a specified permission specification instance.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\build\specification\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\permission\build\api\BuilderInterface;

use liberty_code\role\permission\specification\library\ToolBoxPermSpec;
use liberty_code\role\permission\specification\api\PermSpecInterface;
use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\permission\factory\api\PermissionFactoryInterface;
use liberty_code\role\permission\build\library\ConstBuilder;
use liberty_code\role\permission\build\exception\FactoryInvalidFormatException;
use liberty_code\role\permission\build\specification\library\ConstPermSpecBuilder;
use liberty_code\role\permission\build\specification\exception\PermSpecInvalidFormatException;



/**
 * @method PermSpecInterface getObjPermSpec() Get permission specification object.
 * @method void setObjPermSpec(PermSpecInterface $objPermSpec) Set permission specification object.
 * @method PermissionFactoryInterface getObjFactory() Get permission factory object.
 * @method void setObjFactory(PermissionFactoryInterface $objFactory) Set permission factory object.
 */
class PermSpecBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermSpecInterface $objPermSpec
     * @param PermissionFactoryInterface $objFactory
     */
    public function __construct(
        PermSpecInterface $objPermSpec,
        PermissionFactoryInterface $objFactory
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init permission specification
        $this->setObjPermSpec($objPermSpec);

        // Init permission factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermSpecBuilder::DATA_KEY_DEFAULT_PERM_SPEC))
        {
            $this->__beanTabData[ConstPermSpecBuilder::DATA_KEY_DEFAULT_PERM_SPEC] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }
    }



    /**
     * @inheritdoc
     */
    public function hydratePermissionCollection(PermissionCollectionInterface $objPermissionCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabPermKey = ToolBoxPermSpec::getTabKey($this->getObjPermSpec());

        // Clear permissions from collection, if required
        if($boolClear)
        {
            $objPermissionCollection->removePermissionAll();
        }
        // Else: clear un-existing permissions from collection
        else
        {
            // Run each permission key
            $tabKey = $objPermissionCollection->getTabPermissionKey();
            foreach($tabKey as $strKey)
            {
                // Remove permission, if required
                if(!array_key_exists($strKey, $tabPermKey))
                {
                    $objPermissionCollection->removePermission($strKey);
                }
            }
        }

        // Run each permission key
        $tabPermission = array();
        foreach($tabPermKey as $strPermKey => $tabPermConfig)
        {
            // Get new permission, if required (not already exists)
            if(!$objPermissionCollection->checkPermissionExists($strPermKey))
            {
                $objPermission = $objFactory->getObjPermission($tabPermConfig, $strPermKey);
                $tabPermission[] = $objPermission;
            }
        }

        // Register permissions on collection
        $objPermissionCollection->setTabPermission($tabPermission);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstPermSpecBuilder::DATA_KEY_DEFAULT_PERM_SPEC,
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstPermSpecBuilder::DATA_KEY_DEFAULT_PERM_SPEC:
                    PermSpecInvalidFormatException::setCheck($value);
                    break;

                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}



}