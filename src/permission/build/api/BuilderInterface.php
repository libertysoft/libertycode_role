<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified permission collection instance, with permissions.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\build\api;

use liberty_code\role\permission\api\PermissionCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified permission collection.
     *
     * @param PermissionCollectionInterface $objPermissionCollection
     * @param boolean $boolClear = true
     */
    public function hydratePermissionCollection(PermissionCollectionInterface $objPermissionCollection, $boolClear = true);
}