<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate permission collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate permission collection:
 * [
 *     // Permission configuration 1
 *     Permission factory configuration (@see PermissionFactoryInterface ),
 *
 *     ...,
 *
 *     // Permission configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\permission\build\api\BuilderInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\permission\factory\api\PermissionFactoryInterface;
use liberty_code\role\permission\build\library\ConstBuilder;
use liberty_code\role\permission\build\exception\FactoryInvalidFormatException;
use liberty_code\role\permission\build\exception\DataSrcInvalidFormatException;



/**
 * @method PermissionFactoryInterface getObjFactory() Get permission factory object.
 * @method void setObjFactory(PermissionFactoryInterface $objFactory) Set permission factory object.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionFactoryInterface $objFactory
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        PermissionFactoryInterface $objFactory,
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init permission factory
        $this->setObjFactory($objFactory);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     */
    public function hydratePermissionCollection(PermissionCollectionInterface $objPermissionCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabDataSrc = $this->getTabDataSrc();


        // Run each data source
        $tabPermission = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get new role
            $key = (is_string($key) ? $key : null);
            $objPermission = $objFactory->getObjPermission($tabConfig, $key);

            // Register permission, if found
            if(!is_null($objPermission))
            {
                $tabPermission[] = $objPermission;
            }
            // Throw exception if permission not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear permissions from collection, if required
        if($boolClear)
        {
            $objPermissionCollection->removePermissionAll();
        }

        // Register permissions on collection
        $objPermissionCollection->setTabPermission($tabPermission);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
            ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}

	
	
}