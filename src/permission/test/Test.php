<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/permission/test/PermissionTest.php');

// Use
use liberty_code\role\permission\library\ConstPermission;
use liberty_code\role\permission\model\DefaultPermissionCollection;



// Init var
$objPermissionCollection = new DefaultPermissionCollection();



// Test hydrate permission collection
echo('Test hydrate permission collection: <br />');

echo('Before hydrate: <pre>');
foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
{
    $objPermission = $objPermissionCollection->getObjPermission($strKey);
    echo(sprintf(
        'Permission "%1$s": %2$s',
        $objPermission->getStrPermissionKey(),
        $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
    ));
    echo('<br />');
}
echo('</pre>');

$objPermissionSpecBuilder->hydratePermissionCollection($objPermissionCollection);

echo('After hydrate: <pre>');
foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
{
    $objPermission = $objPermissionCollection->getObjPermission($strKey);
    echo(sprintf(
        'Permission "%1$s": %2$s',
        $objPermission->getStrPermissionKey(),
        $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
    ));
    echo('<br />');
}
echo('</pre>');

echo('<br /><br /><br />');



// Test get
echo('Test get permission: <br />');

try
{
    $objPermissionCollection->setPermissionAllEnable('test');
}
catch (Exception $e)
{
    echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
    echo('<br />');
}
$objPermissionCollection->setPermissionAllEnable(true);

foreach($objPermissionCollection->getTabPermissionKey() as $strPermissionKey)
{
    echo('Test get permission key "'.$strPermissionKey.'": <br />');

    $objPermission = $objPermissionCollection->getObjPermission($strPermissionKey);

    echo('Get permission key: <pre>');var_dump($objPermission->getStrPermissionKey());echo('</pre>');
    echo('Check permission enable: <pre>');var_dump($objPermission->checkPermissionEnable());echo('</pre>');
    echo('Get permission configuration: <pre>');var_dump($objPermission->getTabPermissionConfig());echo('</pre>');
    echo('Check collection permission enable: <pre>');var_dump($objPermissionCollection->checkPermissionEnable($strPermissionKey));echo('</pre>');

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test re-hydrate permission collection
echo('Test re-hydrate permission collection: <br />');

$tabPermission = array(
    $objPermissionFactory->getObjPermission(array(
        ConstPermission::TAB_CONFIG_KEY_KEY => 'test_add_1',
        ConstPermission::TAB_CONFIG_KEY_ENABLE => true
    )),
    $objPermissionFactory->getObjPermission(array(
        ConstPermission::TAB_CONFIG_KEY_KEY => 'test_add_2'
    )),
    $objPermissionFactory->getObjPermission(array(
        ConstPermission::TAB_CONFIG_KEY_KEY => 'test_add_3',
        ConstPermission::TAB_CONFIG_KEY_ENABLE => true
    ))
);
$objPermissionCollection->setTabPermission($tabPermission);

echo('Before hydrate: <pre>');
foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
{
    $objPermission = $objPermissionCollection->getObjPermission($strKey);
    echo(sprintf(
        'Permission "%1$s": %2$s',
        $objPermission->getStrPermissionKey(),
        $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
    ));
    echo('<br />');
}
echo('</pre>');

$objPermissionSpecBuilder->hydratePermissionCollection($objPermissionCollection, false);

echo('After hydrate: <pre>');
foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
{
    $objPermission = $objPermissionCollection->getObjPermission($strKey);
    echo(sprintf(
        'Permission "%1$s": %2$s',
        $objPermission->getStrPermissionKey(),
        $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
    ));
    echo('<br />');
}
echo('</pre>');

echo('<br /><br /><br />');


