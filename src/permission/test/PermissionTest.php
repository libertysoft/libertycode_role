<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/permission/specification/test/PermSpecTest.php');

// Use
use liberty_code\role\permission\specification\test\PermSpecTest;
use liberty_code\role\permission\factory\standard\model\StandardPermissionFactory;
use liberty_code\role\permission\build\model\DefaultBuilder;
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;



// Init permission specification
$objPermSpec = new PermSpecTest();

// Init factory
$objPermissionFactory = new StandardPermissionFactory();

// Init builder
$objPermissionBuilder = new DefaultBuilder($objPermissionFactory);

$objPermissionSpecBuilder = new PermSpecBuilder(
    $objPermSpec,
    $objPermissionFactory
);


