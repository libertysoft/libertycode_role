<?php
/**
 * Description :
 * This class allows to define default permission factory class.
 * Can be consider is base of all permission factory types.
 *
 * Default permission factory uses the following specified configuration, to get and hydrate permission:
 * [
 *     type(optional): "string constant to determine permission type",
 *
 *     ... specific permission configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\role\permission\factory\api\PermissionFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role\permission\factory\library\ConstPermissionFactory;
use liberty_code\role\permission\factory\exception\FactoryInvalidFormatException;
use liberty_code\role\permission\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|PermissionFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|PermissionFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultPermissionFactory extends DefaultFactory implements PermissionFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionFactoryInterface $objFactory = null
     */
    public function __construct(
        PermissionFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init permission factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermissionFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstPermissionFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified permission.
     * Overwrite it to set specific call hydration.
     *
     * @param PermissionInterface $objPermission
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydratePermission(PermissionInterface $objPermission, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstPermissionFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstPermissionFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate permission
        $objPermission->setPermissionConfig($tabConfigFormat);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPermissionFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermissionFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified permission object.
     *
     * @param PermissionInterface $objPermission
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(PermissionInterface $objPermission, array $tabConfigFormat)
    {
        // Init var
        $strPermissionClassPath = $this->getStrPermissionClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strPermissionClassPath)) &&
            ($strPermissionClassPath == get_class($objPermission))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstPermissionFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstPermissionFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of permission,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrPermissionClassPathFromType($strConfigType);



    /**
     * Get string class path of permission engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrPermissionClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrPermissionClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrPermissionClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrPermissionClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrPermissionClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance permission,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|PermissionInterface
     */
    protected function getObjPermissionNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrPermissionClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance permission engine.
     *
     * @param array $tabConfigFormat
     * @param PermissionInterface $objPermission = null
     * @return null|PermissionInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjPermissionEngine(
        array $tabConfigFormat,
        PermissionInterface $objPermission = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objPermission = (
            is_null($objPermission) ?
                $this->getObjPermissionNew($strConfigType) :
                $objPermission
        );

        // Get and hydrate permission, if required
        if(
            (!is_null($objPermission)) &&
            $this->checkConfigIsValid($objPermission, $tabConfigFormat)
        )
        {
            $this->hydratePermission($objPermission, $tabConfigFormat);
            $result = $objPermission;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjPermission(
        array $tabConfig,
        $strConfigKey = null,
        PermissionInterface $objPermission = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjPermissionEngine($tabConfigFormat, $objPermission);

        // Get permission from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjPermission($tabConfig, $strConfigKey, $objPermission);
        }

        // Return result
        return $result;
    }



}