<?php
/**
 * Description :
 * This class allows to define standard permission factory class.
 * Standard permission factory allows to provide and hydrate permission instance.
 *
 * Standard permission factory uses the following specified configuration, to get and hydrate permission:
 * [
 *     -> Configuration key(optional): "permission key"
 *     type(required): "default",
 *     Default permission configuration (@see DefaultPermission )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\factory\standard\model;

use liberty_code\role\permission\factory\model\DefaultPermissionFactory;

use liberty_code\role\permission\library\ConstPermission;
use liberty_code\role\permission\model\DefaultPermission;
use liberty_code\role\permission\factory\standard\library\ConstStandardPermissionFactory;



class StandardPermissionFactory extends DefaultPermissionFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as event key, if required
            if(!array_key_exists(ConstPermission::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstPermission::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrPermissionClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of permission, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardPermissionFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultPermission::class;
                break;
        }

        // Return result
        return $result;
    }

	
	
}