<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\factory\library;



class ConstPermissionFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';



    // Route configuration
    const TAB_CONFIG_KEY_TYPE = 'type';



    // Exception message constants
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following permission factory "%1$s" invalid! It must be null or a factory object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default permission factory configuration standard.';
}