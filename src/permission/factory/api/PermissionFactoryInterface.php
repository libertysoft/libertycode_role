<?php
/**
 * Description :
 * This class allows to describe behavior of permission factory class.
 * Permission factory allows to provide new or specified permission instances,
 * hydrated with a specified configuration,
 * from a set of potential predefined permission types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\factory\api;

use liberty_code\role\permission\api\PermissionInterface;



interface PermissionFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of permission,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrPermissionClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance permission,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param PermissionInterface $objPermission = null
     * @return null|PermissionInterface
     */
    public function getObjPermission(
        array $tabConfig,
        $strConfigKey = null,
        PermissionInterface $objPermission = null
    );
}