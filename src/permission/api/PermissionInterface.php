<?php
/**
 * Description :
 * This class allows to describe behavior of permission class.
 * Permission allows to design a check-able right,
 * to authorize specific operations.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\api;



interface PermissionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check permission enabled.
     *
     * @return boolean
     */
    public function checkPermissionEnable();





	// Methods getters
	// ******************************************************************************

	/**
	 * Get string permission key.
	 *
	 * @return string
	 */
	public function getStrPermissionKey();



    /**
     * Get permission configuration array.
     *
     * @return array
     */
    public function getTabPermissionConfig();





    // Methods setters
    // ******************************************************************************

    /**
     * Set permission configuration array.
     *
     * @param array $tabConfig
     */
    public function setPermissionConfig(array $tabConfig);
}