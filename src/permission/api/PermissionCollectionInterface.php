<?php
/**
 * Description :
 * This class allows to describe behavior of permission collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\api;

use liberty_code\role\subject\api\SubjectInterface;

use liberty_code\role\permission\api\PermissionInterface;



interface PermissionCollectionInterface extends SubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

	/**
	 * Check if permission exists,
     * from specified permission key.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkPermissionExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of permission keys.
	 *
	 * @return array
	 */
	public function getTabPermissionKey();



	/**
	 * Get permission,
     * from specified permission key.
	 * 
	 * @param string $strKey
	 * @return null|PermissionInterface
	 */
	public function getObjPermission($strKey);
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set permission and return its key.
	 * 
	 * @param PermissionInterface $objPermission
	 * @return string
     */
	public function setPermission(PermissionInterface $objPermission);



    /**
     * Set list of permissions (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|static $tabPermission
     * @return array
     */
    public function setTabPermission($tabPermission);



    /**
     * Remove permission and return its instance.
     *
     * @param string $strKey
     * @return PermissionInterface
     */
    public function removePermission($strKey);



    /**
     * Remove all permissions.
     */
    public function removePermissionAll();
}