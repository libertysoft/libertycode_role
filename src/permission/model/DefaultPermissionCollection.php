<?php
/**
 * This class allows to define default permission collection class.
 * key: permission key => permission.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\role\permission\api\PermissionCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\role\permission\api\PermissionInterface;
use liberty_code\role\permission\model\DefaultPermission;
use liberty_code\role\permission\exception\CollectionKeyInvalidFormatException;
use liberty_code\role\permission\exception\CollectionValueInvalidFormatException;



class DefaultPermissionCollection extends DefaultBean implements PermissionCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var PermissionInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrPermissionKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionExists($strKey)
    {
        // Return result
        return (!is_null($this->getObjPermission($strKey)));
    }



    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Init var
        $objPermission = $this->getObjPermission($strKey);
        $result = (
            (!is_null($objPermission)) ?
                $objPermission->checkPermissionEnable() :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabPermissionKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjPermission($strKey)
    {
        // Init var
        $result = null;

        // Try to get permission object, if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CollectionValueInvalidFormatException
     */
    public function setPermission(PermissionInterface $objPermission)
    {
        // Init var
        $strKey = $objPermission->getStrPermissionKey();

        // Register instance
        $this->beanPutData($strKey, $objPermission);

        // return result
        return $strKey;
    }



    /**
     * @inheritdoc
     */
    public function setTabPermission($tabPermission)
    {
        // Init var
        $result = array();

        // Case index array of permissions
        if(is_array($tabPermission))
        {
            // Run all permissions and for each, try to set
            foreach($tabPermission as $permission)
            {
                $strKey = $this->setPermission($permission);
                $result[] = $strKey;
            }
        }
        // Case collection of permissions
        else if($tabPermission instanceof PermissionCollectionInterface)
        {
            // Run all permissions and for each, try to set
            foreach($tabPermission->getTabPermissionKey() as $strKey)
            {
                $objPermission = $tabPermission->getObjPermission($strKey);
                $strKey = $this->setPermission($objPermission);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removePermission($strKey)
    {
        // Init var
        $result = $this->getObjPermission($strKey);

        // Remove permission
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removePermissionAll()
    {
        // Ini var
        $tabKey = $this->getTabPermissionKey();

        foreach($tabKey as $strKey)
        {
            $this->removePermission($strKey);
        }
    }



    /**
     * Set all permissions enabled.
     *
     * @param boolean $boolEnable
     */
    public function setPermissionAllEnable($boolEnable)
    {
        // Run all permissions
        foreach($this->getTabPermissionKey() as $strKey)
        {
            $objPermission = $this->getObjPermission($strKey);

            // Set permission enabled
            if($objPermission instanceof DefaultPermission)
            {
                $objPermission->setPermissionEnable($boolEnable);
            }
        }
    }



}