<?php
/**
 * This class allows to define default permission class.
 * Can be consider is base of all permission types.
 *
 * Permission uses the following specified configuration:
 * [
 *     key(required): "string permission key",
 *
 *     enable(optional: got false if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\permission\api\PermissionInterface;

use liberty_code\role\permission\library\ConstPermission;
use liberty_code\role\permission\exception\ConfigInvalidFormatException;



class DefaultPermission extends FixBean implements PermissionInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     */
    public function __construct(array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setPermissionConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermission::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstPermission::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPermission::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermission::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable()
    {
        // Init var
        $tabConfig = $this->getTabPermissionConfig();
        $result = (
            (array_key_exists(ConstPermission::TAB_CONFIG_KEY_ENABLE, $tabConfig)) ?
                (intval($tabConfig[ConstPermission::TAB_CONFIG_KEY_ENABLE]) != 0) :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrPermissionKey()
    {
        // Init var
        $tabConfig = $this->getTabPermissionConfig();
        $result = $tabConfig[ConstPermission::TAB_CONFIG_KEY_KEY];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabPermissionConfig()
    {
        // Return result
        return $this->beanGet(ConstPermission::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setPermissionConfig(array $tabConfig)
    {
        $this->beanSet(ConstPermission::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * Set permission enabled.
     *
     * @param boolean $boolEnable
     */
    public function setPermissionEnable($boolEnable)
    {
        // Init var
        $tabConfig = $this->getTabPermissionConfig();

        // Set force access option
        $tabConfig[ConstPermission::TAB_CONFIG_KEY_ENABLE] = $boolEnable;

        // Set configuration
        $this->setPermissionConfig($tabConfig);
    }



}