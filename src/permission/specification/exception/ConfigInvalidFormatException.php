<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\exception;

use Exception;

use liberty_code\role\permission\specification\library\ConstPermSpec;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPermSpec::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache key
            (
                (!isset($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_KEY])) ||
                (
                    is_string($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_KEY]) &&
                    (trim($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_KEY]) != '')
                )
            ) &&

            // Check valid cache set configuration
            (
                (!isset($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG])) ||
                is_array($config[ConstPermSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @param array $tabFixConfig
     * @return boolean
     * @throws static
     */
    public static function setCheck($config, array $tabFixConfig)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&
            ($tabFixConfig === array_intersect_key($config, $tabFixConfig)) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}