<?php
/**
 * Description :
 * This class allows to define default permission specification class.
 * Can be consider is base of all permission specification types.
 *
 * Default permission specification uses the following specified configuration:
 * [
 *     cache_require(optional: got true, if not found): true / false,
 *
 *     cache_key(optional: got @see ConstPermSpec::CACHE_DEFAULT_KEY, if not found):
 *         "string key used on cache repository",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\permission\specification\api\PermSpecInterface;

use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\role\permission\specification\library\ConstPermSpec;
use liberty_code\role\permission\specification\exception\ConfigInvalidFormatException;
use liberty_code\role\permission\specification\exception\CacheRepoInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class DefaultPermSpec extends FixBean implements PermSpecInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     */
    public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstPermSpec::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstPermSpec::DATA_KEY_DEFAULT_CONFIG] = $this->getTabFixConfig();
        }

        if(!$this->beanExists(ConstPermSpec::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstPermSpec::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstPermSpec::DATA_KEY_DEFAULT_CONFIG,
            ConstPermSpec::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermSpec::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value, $this->getTabFixConfig());
                    break;

                case ConstPermSpec::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    public function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstPermSpec::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     * Overwrite it to implement specific configuration.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array();
    }



    /**
     * Get specified cache key.
     *
     * @return string
     */
    public function getStrCacheKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstPermSpec::TAB_CONFIG_KEY_CACHE_KEY, $tabConfig) ?
                $tabConfig[ConstPermSpec::TAB_CONFIG_KEY_CACHE_KEY] :
                ConstPermSpec::CACHE_DEFAULT_KEY
        );

        // Return result
        return $result;
    }



    /**
     * Get array of permission keys engine.
     *
     * Return array format:
     * @see getTabKey() return array format.
     *
     * @return array
     */
    abstract protected function getTabKeyEngine();



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $tabCacheConfig = (
            isset($tabConfig[ConstPermSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                $tabConfig[ConstPermSpec::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                null
        );
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheKey(),
                    function() {return $this->getTabKeyEngine();},
                    $tabCacheConfig
                ) :
                $this->getTabKeyEngine()
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        // Format configuration
        $tabConfig = array_merge($this->getTabFixConfig(), $tabConfig);

        // Set configuration
        $this->beanSet(ConstPermSpec::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}