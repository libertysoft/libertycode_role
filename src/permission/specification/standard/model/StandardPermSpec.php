<?php
/**
 * Description :
 * This class allows to define standard permission specification class.
 * Standard permission specification contains all information,
 * to provide a scope of permission keys.
 *
 * Standard permission specification uses the following specified configuration:
 * [
 *     Default permission specification configuration,
 *
 *     permission_key(required): [
 *         @see StandardPermSpec::getTabKey() return array format
 *     ]
 * ]
 *
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\standard\model;

use liberty_code\role\permission\specification\model\DefaultPermSpec;

use liberty_code\role\permission\specification\library\ConstPermSpec;
use liberty_code\role\permission\specification\standard\library\ConstStandardPermSpec;
use liberty_code\role\permission\specification\standard\exception\ConfigInvalidFormatException;



class StandardPermSpec extends DefaultPermSpec
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstPermSpec::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabKeyEngine()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstStandardPermSpec::TAB_CONFIG_KEY_PERMISSION_KEY];

        // Return result
        return $result;
    }



}