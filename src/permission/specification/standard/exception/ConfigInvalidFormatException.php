<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\standard\exception;

use Exception;

use liberty_code\role\permission\specification\standard\library\ConstStandardPermSpec;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardPermSpec::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init permission keys array check function
        $checkTabPermKeyIsValid = function(array $tabKey)
        {
            $result = true;
            $tabRegisterKey = array();

            // Run all keys
            $tabKeyKey = array_keys($tabKey);
            for($intCpt = 0; ($intCpt < count($tabKeyKey)) && $result; $intCpt++)
            {
                // Get info
                $keyKey = $tabKeyKey[$intCpt];
                $keyValue = $tabKey[$keyKey];
                $strKey = (is_string($keyKey) ? $keyKey : $keyValue);
                $tabConfig = (is_string($keyValue) ? array() : $keyValue);

                // Check key
                $result =
                    // Check valid permission key
                    is_string($strKey) &&
                    (trim($strKey) != '') &&
                    (!in_array($strKey, $tabRegisterKey)) &&

                    // Check valid permission configuration
                    is_array($tabConfig);

                // Register unique info
                $tabRegisterKey[] = $strKey;
            }


            return $result;
        };

        // Init var
        $result =
            // Check valid permission keys array
            isset($config[ConstStandardPermSpec::TAB_CONFIG_KEY_PERMISSION_KEY]) &&
            is_array($config[ConstStandardPermSpec::TAB_CONFIG_KEY_PERMISSION_KEY]) &&
            $checkTabPermKeyIsValid($config[ConstStandardPermSpec::TAB_CONFIG_KEY_PERMISSION_KEY]);

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}