<?php

namespace liberty_code\role\permission\specification\test;

use liberty_code\role\permission\specification\standard\model\StandardPermSpec;



class PermSpecTest extends StandardPermSpec
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        return array(
            'permission_key' => [
                'test_get',
                'test_set' => [
                    'enable' => true,
                    'key-test-1' => 'Value test 1'
                ],
                'test_delete' => [
                    'key-test-1' => 'Value test 1',
                    'key-test-2' => 'Value test 2'
                ]
            ]
        );
    }



}