<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/permission/specification/test/PermSpecTest.php');

// Use
use liberty_code\role\permission\specification\library\ToolBoxPermSpec;
use liberty_code\role\permission\specification\test\PermSpecTest;



// Init var
$objPermSpec = new PermSpecTest(array(
    'cache_require' => true
));



// Test get permission keys
$tabKey = $objPermSpec->getTabKey();
echo('Test get permission keys: <pre>');var_dump($tabKey);echo('</pre>');

$tabKey = ToolBoxPermSpec::getTabKey($objPermSpec);
echo('Test get formatted permission keys: <pre>');var_dump($tabKey);echo('</pre>');

echo('<br /><br /><br />');


