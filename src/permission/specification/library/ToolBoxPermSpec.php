<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\role\permission\specification\api\PermSpecInterface;



class ToolBoxPermSpec extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get associative array of permission keys.
     * from specified permission specification.
     *
     * Return array format:
     * [
     *     // Permission key 1
     *     'string permission key 1' (required) => [
     *         Permission configuration array (optional: got [] if not found)
     *     ],
     *
     *     ...
     *
     *     // Permission key N
     * `   ...
     * ]
     *
     * @param PermSpecInterface $objPermSpec
     * @return array
     */
    public static function getTabKey(PermSpecInterface $objPermSpec)
    {
        // Init var
        $result = array();
        $tabKey = $objPermSpec->getTabKey();

        // Run each permission key
        foreach($tabKey as $strKey => $tabConfig)
        {
            $strKey = (is_string($tabConfig) ? $tabConfig : $strKey);
            $tabConfig = (is_array($tabConfig) ? $tabConfig : array());

            // Register permission key, configuration
            $result[$strKey] = $tabConfig;
        }

        // Return result
        return $result;
    }



}