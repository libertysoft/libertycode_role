<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\library;



class ConstPermSpec
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_KEY = 'cache_key';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';

    // Cache configuration
    const CACHE_DEFAULT_KEY = 'permission_key';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default permission specification configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';



}