<?php
/**
 * Description :
 * This class allows to describe behavior of permission specification class.
 * Permission specification allows to provide a scope of permission keys,
 * used in specific access system.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\permission\specification\api;



interface PermSpecInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get array of permission keys.
     *
     * Return array format:
     * [
     *     // Permission key 1
     *     'string permission key 1' (required),
     *
     *     OR
     *
     *     'string permission key 1' (required) => [
     *         Permission configuration array (optional: got [] if not found)
     *     ],
     *
     *     ...
     *
     *     // Permission key N
     * `   ...
     * ]
     *
     * @return array
     */
    public function getTabKey();
}