<?php
/**
 * Description :
 * This class allows to describe behavior of subject class.
 * Subject is item, where permissions enabled states can be checked.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\subject\api;



interface SubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified permission enabled.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkPermissionEnable($strKey);
}