<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\subject\library;

use liberty_code\role\subject\api\SubjectInterface;



class ToolBoxSubject
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified permission enabled,
     * from specified index array of subjects.
     *
     * @param string $strKey
     * @param SubjectInterface[] $tabSubject
     * @return boolean
     */
    public static function checkPermissionEnable($strKey, array $tabSubject)
    {
        // Init var
        $result = false;
        $tabSubject = array_values($tabSubject);

        // Run each role
        for($intCpt = 0; ($intCpt < count($tabSubject)) && (!$result); $intCpt++)
        {
            $objSubject = $tabSubject[$intCpt];

            // Check permission enabled
            $result = (
                ($objSubject instanceof SubjectInterface) &&
                $objSubject->checkPermissionEnable($strKey)
            );
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified permission enabled,
     * from specified list of subjects.
     *
     * @param string $strKey
     * @param SubjectInterface ...$subject
     * @return boolean
     */
    public static function checkPermissionEnableFromList($strKey, ...$subject)
    {
        // Return result
        return static::checkPermissionEnable($strKey, $subject);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified index array of permission keys enabled,
     * from specified index array of subjects.
     *
     * @param array $tabKey
     * @param SubjectInterface[] $tabSubject
     * @return array
     */
    public static function getTabPermissionKeyEnable(
        array $tabKey,
        array $tabSubject
    )
    {
        // Init var
        $result = array();
        $tabKey = array_values($tabKey);

        // Run each permission
        foreach($tabKey as $strKey)
        {
            // Check permission enabled
            if(static::checkPermissionEnable($strKey, $tabSubject))
            {
                $result[] = $strKey;
            }
        }

        // Return result
        return $result;
    }



}