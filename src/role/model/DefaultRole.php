<?php
/**
 * This class allows to define default role class.
 * Can be consider is base of all role types.
 *
 * Role uses the following specified configuration:
 * [
 *     name(required): "string name"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\model;

use liberty_code\role\permission\subject\model\DefaultPermissionSubject;
use liberty_code\role\role\api\RoleInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\role\library\ConstRole;
use liberty_code\role\role\exception\ConfigInvalidFormatException;



class DefaultRole extends DefaultPermissionSubject implements RoleInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionCollectionInterface $objPermissionCollection = null
     * @param array $tabConfig = null
     */
    public function __construct(PermissionCollectionInterface $objPermissionCollection = null, array $tabConfig = null)
    {
        // Call parent constructor
        parent::__construct($objPermissionCollection);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setRoleConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRole::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRole::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRole::DATA_KEY_DEFAULT_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRole::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrRoleName()
    {
        // Init var
        $tabConfig = $this->getTabRoleConfig();
        $result = $tabConfig[ConstRole::TAB_CONFIG_KEY_NAME];

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRoleConfig()
    {
        // Return result
        return $this->beanGet(ConstRole::DATA_KEY_DEFAULT_CONFIG);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setRoleConfig(array $tabConfig)
    {
        $this->beanSet(ConstRole::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}