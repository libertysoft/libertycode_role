<?php
/**
 * This class allows to define default role collection class.
 * key: role name => role.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\role\role\api\RoleCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\role\role\api\RoleInterface;
use liberty_code\role\role\exception\CollectionKeyInvalidFormatException;
use liberty_code\role\role\exception\CollectionValueInvalidFormatException;



class DefaultRoleCollection extends DefaultBean implements RoleCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var RoleInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrRoleName())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkRoleExists($strName)
    {
        // Return result
        return (!is_null($this->getObjRole($strName)));
    }



    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Init var
        $result = false;
        $tabRoleName = array_values($this->getTabRoleName());

        // Run each role
        for($intCpt = 0; ($intCpt < count($tabRoleName)) && (!$result); $intCpt++)
        {
            $strName = $tabRoleName[$intCpt];
            $objRole = $this->getObjRole($strName);

            // Check permission enabled
            $result = $objRole->checkPermissionEnable($strKey);
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabRoleName()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     */
    public function getObjRole($strName)
    {
        // Init var
        $result = null;

        // Try to get role object if found
        try
        {
            if($this->beanDataExists($strName))
            {
                $result = $this->beanGetData($strName);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws CollectionValueInvalidFormatException
     */
    public function setRole(RoleInterface $objRole)
    {
        // Init var
        $strName = $objRole->getStrRoleName();

        // Register instance
        $this->beanPutData($strName, $objRole);

        // return result
        return $strName;
    }



    /**
     * @inheritdoc
     */
    public function setTabRole($tabRole)
    {
        // Init var
        $result = array();

        // Case index array of roles
        if(is_array($tabRole))
        {
            // Run all roles and for each, try to set
            foreach($tabRole as $role)
            {
                $strName = $this->setRole($role);
                $result[] = $strName;
            }
        }
        // Case collection of roles
        else if($tabRole instanceof RoleCollectionInterface)
        {
            // Run all roles and for each, try to set
            foreach($tabRole->getTabRoleName() as $strName)
            {
                $objRole = $tabRole->getObjRole($strName);
                $strName = $this->setRole($objRole);
                $result[] = $strName;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRole($strName)
    {
        // Init var
        $result = $this->getObjRole($strName);

        // Remove role
        $this->beanRemoveData($strName);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeRoleAll()
    {
        // Ini var
        $tabName = $this->getTabRoleName();

        foreach($tabName as $strName)
        {
            $this->removeRole($strName);
        }
    }



}