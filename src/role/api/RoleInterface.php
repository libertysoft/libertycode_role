<?php
/**
 * Description :
 * This class allows to describe behavior of role class.
 * Role allows to design a named group of permissions.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\api;

use liberty_code\role\permission\subject\api\PermissionSubjectInterface;



interface RoleInterface extends PermissionSubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string role name.
     *
     * @return string
     */
    public function getStrRoleName();



    /**
     * Get role configuration array.
     *
     * @return array
     */
    public function getTabRoleConfig();





    // Methods setters
    // ******************************************************************************

    /**
     * Set role configuration array.
     *
     * @param array $tabConfig
     */
    public function setRoleConfig(array $tabConfig);
}