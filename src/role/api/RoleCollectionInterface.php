<?php
/**
 * Description :
 * This class allows to describe behavior of role collection class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\api;

use liberty_code\role\subject\api\SubjectInterface;

use liberty_code\role\role\api\RoleInterface;



interface RoleCollectionInterface extends SubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

	/**
	 * Check if role exists,
     * from specified role name.
	 * 
	 * @param string $strName
	 * @return boolean
	 */
	public function checkRoleExists($strName);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get index array of role names.
	 *
	 * @return array
	 */
	public function getTabRoleName();



	/**
	 * Get role,
     * from specified role name.
	 * 
	 * @param string $strName
	 * @return null|RoleInterface
	 */
	public function getObjRole($strName);
	




	// Methods setters
	// ******************************************************************************

	/**
	 * Set role and return its name.
	 * 
	 * @param RoleInterface $objRole
	 * @return string
     */
	public function setRole(RoleInterface $objRole);



    /**
     * Set list of roles (index array or collection) and
     * return its list of names (index array).
     *
     * @param array|static $tabRole
     * @return array
     */
    public function setTabRole($tabRole);



    /**
     * Remove role and return its instance.
     *
     * @param string $strName
     * @return RoleInterface
     */
    public function removeRole($strName);



    /**
     * Remove all roles.
     */
    public function removeRoleAll();
}