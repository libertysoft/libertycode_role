<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/role/test/RoleTest.php');

// Use
use liberty_code\role\subject\library\ToolBoxSubject;
use liberty_code\role\permission\model\DefaultPermissionCollection;
use liberty_code\role\role\model\DefaultRoleCollection;
use liberty_code\role\role\subject\model\DefaultRolePermissionSubject;



// Init var
$objPermissionCollection = new DefaultPermissionCollection();
$objRoleCollection = new DefaultRoleCollection();
$objSubject = new DefaultRolePermissionSubject($objPermissionCollection, $objRoleCollection);



// Hydrate permissions
$tabDataSrc = array(
    'test_get' => [
        'enable' => true
    ],
    'test_set' => [
        'enable' => true
    ],
    'test_add_1' => [
        'enable' => true
    ],
    'test_add_2' => [
        'enable' => false
    ],
    'test_add_3' => [
        'enable' => true
    ]
);
$objPermissionBuilder->setTabDataSrc($tabDataSrc);
$objPermissionBuilder->hydratePermissionCollection($objPermissionCollection);



// Hydrate roles
$tabDataSrc = array(
    'role_1' => [
        'permission_data_src' => [
            'test_set' => [
                'enable' => false
            ],
            'test_delete' => [
                'enable' => true
            ]
        ]
    ],
    'role_2' => [
        'permission_data_src' => [
            'test_set' => [
                'enable' => true
            ]
        ]
    ],
    'role_3' => []
);
$objRoleBuilder->setTabDataSrc($tabDataSrc);
$objRoleBuilder->hydrateRoleCollection($objRoleCollection);



// Test check permission enable
echo('Test check permission enable: <br />');
$tabKey = array(
    'test_get', // Ok (via permissions)
    'test_set', // Ok
    'test_delete', // Ok (via roles: role_1)
    'test_add_1', // Ok (via permissions)
    'test_add_2', // Ko: Found, disable
    'test_add_3', // Ok (via permissions)
    'test' // Ko: Not found
);

foreach($tabKey as $strKey)
{
    echo('Check permission "'.$strKey.'" enable: <pre>');
    var_dump($objSubject->checkPermissionEnable($strKey));
    var_dump(ToolBoxSubject::checkPermissionEnableFromList(
        $strKey,
        $objSubject->getObjPermissionCollection(),
        $objSubject->getObjRoleCollection()
    ));
    echo('</pre>');
}

echo('<br /><br /><br />');



// Test get permissions enable
echo('Get permissions enable: <pre>');
var_dump(ToolBoxSubject::getTabPermissionKeyEnable(
    $tabKey,
    array(
        $objSubject->getObjPermissionCollection(),
        $objSubject->getObjRoleCollection()
    )
));
echo('</pre>');

echo('<br /><br /><br />');



// Test check role exists
echo('Test check role exists: <br />');
$tabName = array(
    'role_1', // Ok
    'role_2', // Ok
    'role_3', // Ok
    'role_test' // Ko: Not found
);

foreach($tabName as $strName)
{
    echo('Check role "'.$strName.'" exists: <pre>');
    var_dump($objSubject->checkRoleExists($strName));
    echo('</pre>');
}

echo('<br /><br /><br />');


