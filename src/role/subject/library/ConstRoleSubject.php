<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\library;



class ConstRoleSubject
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_ROLE_COLLECTION = 'objRoleCollection';



    // Exception message constants
    const EXCEPT_MSG_ROLE_COLLECTION_INVALID_FORMAT =
        'Following role collection "%1$s" invalid! It must be a role collection object.';
}