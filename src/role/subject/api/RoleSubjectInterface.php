<?php
/**
 * Description :
 * This class allows to describe behavior of role subject class.
 * Role subject is subject, where roles can be attributed.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\api;

use liberty_code\role\subject\api\SubjectInterface;

use liberty_code\role\role\api\RoleCollectionInterface;



interface RoleSubjectInterface extends SubjectInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified role exists.
     *
     * @param string $strName
     * @return boolean
     */
    public function checkRoleExists($strName);





	// Methods getters
	// ******************************************************************************

    /**
     * Get role collection object.
     *
     * @return RoleCollectionInterface
     */
    public function getObjRoleCollection();





    // Methods setters
    // ******************************************************************************

    /**
     * Set role collection object.
     *
     * @param RoleCollectionInterface $objRoleCollection
     */
    public function setRoleCollection(RoleCollectionInterface $objRoleCollection);
}