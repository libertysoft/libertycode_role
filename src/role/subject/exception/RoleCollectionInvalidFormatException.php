<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\exception;

use liberty_code\role\role\api\RoleCollectionInterface;
use liberty_code\role\role\subject\library\ConstRoleSubject;



class RoleCollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $roleCollection
     */
	public function __construct($roleCollection)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRoleSubject::EXCEPT_MSG_ROLE_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($roleCollection), 0, 50, "...")
        );
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified role collection has valid format.
     *
     * @param mixed $roleCollection
     * @return boolean
     * @throws static
     */
    static public function setCheck($roleCollection)
    {
        // Init var
        $result = (
            (is_null($roleCollection)) ||
            ($roleCollection instanceof RoleCollectionInterface)
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($roleCollection);
        }

        // Return result
        return $result;
    }
	
	
	
}