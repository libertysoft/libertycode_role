<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\role\role\build\api\BuilderInterface;
use liberty_code\role\role\subject\api\RoleSubjectInterface;



class ToolBoxRoleBuilder extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate role collections,
     * from specified subject roles|array of subjects roles,
     * with specified role builder|array of role builders.
     *
     * @param RoleSubjectInterface|RoleSubjectInterface[] $subject
     * @param BuilderInterface|BuilderInterface[] $roleBuilder
     * @param boolean $boolClear = true
     */
    public static function hydrateRoleCollection(
        $subject,
        $roleBuilder,
        $boolClear = true
    )
    {
        // Init var
        $tabSubject = (is_array($subject)? array_values($subject) : array($subject));
        $tabRoleBuilder = (is_array($roleBuilder)? array_values($roleBuilder) : array($roleBuilder));
        $boolClear = (is_bool($boolClear) ? $boolClear : true);

        // Run each subject
        foreach($tabSubject as $objSubject)
        {
            if($objSubject instanceof RoleSubjectInterface)
            {
                // Run each role builder
                foreach($tabRoleBuilder as $objRoleBuilder)
                {
                    if($objRoleBuilder instanceof BuilderInterface)
                    {
                        // Hydrate subject role collection
                        $objRoleBuilder->hydrateRoleCollection(
                            $objSubject->getObjRoleCollection(),
                            $boolClear
                        );
                    }
                }
            }
        }
    }



}