<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\build\permission\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\role\permission\build\api\BuilderInterface;
use liberty_code\role\permission\subject\build\library\ToolBoxPermissionBuilder as  BaseToolBoxPermissionBuilder;
use liberty_code\role\role\subject\api\RoleSubjectInterface;



class ToolBoxPermissionBuilder extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate permission collections,
     * from specified subject roles|array of subjects roles,
     * with specified permission builder|array of permission builders.
     *
     * @param RoleSubjectInterface|RoleSubjectInterface[] $subject
     * @param BuilderInterface|BuilderInterface[] $permissionBuilder
     * @param boolean $boolClear = true
     */
    public static function hydratePermissionCollection(
        $subject,
        $permissionBuilder,
        $boolClear = true
    )
    {
        // Init var
        $tabSubject = (is_array($subject)? array_values($subject) : array($subject));

        // Run each subject
        foreach($tabSubject as $objSubject)
        {
            if($objSubject instanceof RoleSubjectInterface)
            {
                // Get array of roles
                $objRoleCollection = $objSubject->getObjRoleCollection();
                $tabRole = array();
                foreach($objRoleCollection->getTabRoleName() as $strRoleNm)
                {
                    $tabRole[] = $objRoleCollection->getObjRole($strRoleNm);
                }

                // Hydrate permission collection of roles
                BaseToolBoxPermissionBuilder::hydratePermissionCollection(
                    $tabRole,
                    $permissionBuilder,
                    $boolClear
                );
            }
        }
    }



}