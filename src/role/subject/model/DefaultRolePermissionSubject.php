<?php
/**
 * This class allows to define default role permission subject class.
 * Can be consider is base of all role permission subject types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\model;

use liberty_code\role\permission\subject\model\DefaultPermissionSubject;
use liberty_code\role\role\subject\api\RoleSubjectInterface;

use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\role\api\RoleCollectionInterface;
use liberty_code\role\role\subject\library\ConstRoleSubject;
use liberty_code\role\role\subject\exception\RoleCollectionInvalidFormatException;



class DefaultRolePermissionSubject extends DefaultPermissionSubject implements RoleSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PermissionCollectionInterface $objPermissionCollection = null
     * @param RoleCollectionInterface $objRoleCollection = null
     */
    public function __construct(
        PermissionCollectionInterface $objPermissionCollection = null,
        RoleCollectionInterface $objRoleCollection = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init permission collection, if required
        if(!is_null($objPermissionCollection))
        {
            $this->setPermissionCollection($objPermissionCollection);
        }

        // Init role collection, if required
        if(!is_null($objRoleCollection))
        {
            $this->setRoleCollection($objRoleCollection);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION))
        {
            $this->__beanTabData[ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION:
                    RoleCollectionInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Return result
        return
            $this->getObjRoleCollection()->checkPermissionEnable($strKey) ||
            parent::checkPermissionEnable($strKey);
    }



    /**
     * @inheritdoc
     */
    public function checkRoleExists($strName)
    {
        // Return result
        return $this->getObjRoleCollection()->checkRoleExists($strName);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjRoleCollection()
    {
        // Return result
        return $this->beanGet(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setRoleCollection(RoleCollectionInterface $objRoleCollection)
    {
        $this->beanSet(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION, $objRoleCollection);
    }



}