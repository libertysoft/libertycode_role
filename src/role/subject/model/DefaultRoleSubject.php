<?php
/**
 * This class allows to define default role subject class.
 * Can be consider is base of all role subject types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\subject\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\role\subject\api\RoleSubjectInterface;

use liberty_code\role\role\api\RoleCollectionInterface;
use liberty_code\role\role\subject\library\ConstRoleSubject;
use liberty_code\role\role\subject\exception\RoleCollectionInvalidFormatException;



class DefaultRoleSubject extends FixBean implements RoleSubjectInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RoleCollectionInterface $objRoleCollection = null
     */
    public function __construct(RoleCollectionInterface $objRoleCollection = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init role collection, if required
        if(!is_null($objRoleCollection))
        {
            $this->setRoleCollection($objRoleCollection);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION))
        {
            $this->__beanTabData[ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION:
                    RoleCollectionInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkPermissionEnable($strKey)
    {
        // Return result
        return $this->getObjRoleCollection()->checkPermissionEnable($strKey);
    }



    /**
     * @inheritdoc
     */
    public function checkRoleExists($strName)
    {
        // Return result
        return $this->getObjRoleCollection()->checkRoleExists($strName);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjRoleCollection()
    {
        // Return result
        return $this->beanGet(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setRoleCollection(RoleCollectionInterface $objRoleCollection)
    {
        $this->beanSet(ConstRoleSubject::DATA_KEY_DEFAULT_ROLE_COLLECTION, $objRoleCollection);
    }



}