<?php
/**
 * Description :
 * This class allows to define standard role factory class.
 * Standard role factory allows to provide and hydrate role instance.
 *
 * Standard role factory uses the following specified configuration, to get and hydrate role:
 * [
 *     -> Configuration key(optional): "role name"
 *     type(required): "default",
 *     Default role configuration (@see DefaultRole )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\factory\standard\model;

use liberty_code\role\role\factory\model\DefaultRoleFactory;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\role\permission\api\PermissionCollectionInterface;
use liberty_code\role\permission\model\DefaultPermissionCollection;
use liberty_code\role\role\library\ConstRole;
use liberty_code\role\role\model\DefaultRole;
use liberty_code\role\role\factory\standard\library\ConstStandardRoleFactory;



class StandardRoleFactory extends DefaultRoleFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as event key, if required
            if(!array_key_exists(ConstRole::TAB_CONFIG_KEY_NAME, $result))
            {
                $result[ConstRole::TAB_CONFIG_KEY_NAME] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrRoleClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of role, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardRoleFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultRole::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjRoleNew($strConfigType)
    {
        // Init var
        $result = parent::getObjRoleNew($strConfigType);
        $objPermissionCollection = $result->getObjPermissionCollection();

        if(
            is_null($result) ||
            (!is_object($objPermissionCollection)) ||
            (!($objPermissionCollection instanceof PermissionCollectionInterface))
        )
        {
            $objPermissionCollection = $this->getObjInstance(DefaultPermissionCollection::class);
            $objPermissionCollection = (
                (is_null($objPermissionCollection)) ?
                    ToolBoxReflection::getObjInstance(DefaultPermissionCollection::class) :
                    $objPermissionCollection
            );

            // Get class path of role, from type
            switch($strConfigType)
            {
                case null:
                case ConstStandardRoleFactory::CONFIG_TYPE_DEFAULT:
                    $result = new DefaultRole($objPermissionCollection);
                    break;
            }
        }

        // Return result
        return $result;
    }



}