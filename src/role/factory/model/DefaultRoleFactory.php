<?php
/**
 * Description :
 * This class allows to define default role factory class.
 * Can be consider is base of all role factory types.
 *
 * Default role factory uses the following specified configuration, to get and hydrate role:
 * [
 *     type(optional): "string constant to determine role type",
 *
 *     ... specific role configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\role\role\factory\api\RoleFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\role\role\api\RoleInterface;
use liberty_code\role\role\factory\library\ConstRoleFactory;
use liberty_code\role\role\factory\exception\FactoryInvalidFormatException;
use liberty_code\role\role\factory\exception\ConfigInvalidFormatException;



/**
 * @method null|RoleFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|RoleFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultRoleFactory extends DefaultFactory implements RoleFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RoleFactoryInterface $objFactory = null
     */
    public function __construct(
        RoleFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init role factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRoleFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstRoleFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified role.
     * Overwrite it to set specific call hydration.
     *
     * @param RoleInterface $objRole
     * @param array $tabConfigFormat
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateRole(RoleInterface $objRole, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstRoleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstRoleFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate role
        $objRole->setRoleConfig($tabConfigFormat);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRoleFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRoleFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified role object.
     *
     * @param RoleInterface $objRole
     * @param array $tabConfigFormat
     * @return boolean
     * @throws ConfigInvalidFormatException
     */
    protected function checkConfigIsValid(RoleInterface $objRole, array $tabConfigFormat)
    {
        // Init var
        $strRoleClassPath = $this->getStrRoleClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strRoleClassPath)) &&
            ($strRoleClassPath == get_class($objRole))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = null;

        // Get type, if found
        if(array_key_exists(ConstRoleFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            $result = $tabConfigFormat[ConstRoleFactory::TAB_CONFIG_KEY_TYPE];
        }

        // Return result
        return $result;
    }



    /**
     * Get string class path of role,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrRoleClassPathFromType($strConfigType);



    /**
     * Get string class path of role engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrRoleClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrRoleClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRoleClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrRoleClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrRoleClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance role,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|RoleInterface
     */
    protected function getObjRoleNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrRoleClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance role engine.
     *
     * @param array $tabConfigFormat
     * @param RoleInterface $objRole = null
     * @return null|RoleInterface
     * @throws ConfigInvalidFormatException
     */
    protected function getObjRoleEngine(
        array $tabConfigFormat,
        RoleInterface $objRole = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objRole = (
            is_null($objRole) ?
                $this->getObjRoleNew($strConfigType) :
                $objRole
        );

        // Get and hydrate role, if required
        if(
            (!is_null($objRole)) &&
            $this->checkConfigIsValid($objRole, $tabConfigFormat)
        )
        {
            $this->hydrateRole($objRole, $tabConfigFormat);
            $result = $objRole;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjRole(
        array $tabConfig,
        $strConfigKey = null,
        RoleInterface $objRole = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjRoleEngine($tabConfigFormat, $objRole);

        // Get role from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjRole($tabConfig, $strConfigKey, $objRole);
        }

        // Return result
        return $result;
    }



}