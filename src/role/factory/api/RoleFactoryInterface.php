<?php
/**
 * Description :
 * This class allows to describe behavior of role factory class.
 * Role factory allows to provide new or specified role instances,
 * hydrated with a specified configuration,
 * from a set of potential predefined role types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\factory\api;

use liberty_code\role\role\api\RoleInterface;



interface RoleFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of role,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrRoleClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified object instance role,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param RoleInterface $objRole = null
     * @return null|RoleInterface
     */
    public function getObjRole(
        array $tabConfig,
        $strConfigKey = null,
        RoleInterface $objRole = null
    );
}