<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Properties
    const DATA_KEY_DEFAULT_FACTORY = 'objFactory';
    const DATA_KEY_DEFAULT_PERMISSION_BUILDER = 'tabPermissionBuilder';
    const DATA_KEY_DEFAULT_DATA_SRC = 'tabDataSrc';



    // Configuration
    const TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC = 'permission_data_src';



    // Exception message
    const EXCEPT_MSG_FACTORY_INVALID_FORMAT = 'Following role factory "%1$s" invalid! It must be null or a factory object.';
    const EXCEPT_MSG_PERMISSION_BUILDER_FORMAT =
        'Following permission builder array "%1$s" invalid! It must be an index array of permission builders object.';
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be an array and following the default role collection builder data source standard.';
}