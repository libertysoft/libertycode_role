<?php
/**
 * Description :
 * This class allows to describe behavior of builder class.
 * Builder allows to populate specified role collection instance, with roles.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\build\api;

use liberty_code\role\role\api\RoleCollectionInterface;



interface BuilderInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************


    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified role collection.
     *
     * @param RoleCollectionInterface $objRoleCollection
     * @param boolean $boolClear = true
     */
    public function hydrateRoleCollection(RoleCollectionInterface $objRoleCollection, $boolClear = true);
}