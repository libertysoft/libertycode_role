<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\build\exception;

use liberty_code\role\role\build\library\ConstBuilder;



class DataSrcInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
     */
	public function __construct($dataSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
		(
			ConstBuilder::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
			mb_strimwidth(strval($dataSrc), 0, 10, "...")
		);
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified data source has valid format.
     *
     * @param array $tabDataSrc
     * @return boolean
     */
    protected static function checkDataSrcIsValid(array $tabDataSrc)
    {
        $result = true;
        $tabDataSrc = array_values($tabDataSrc);

        // Check each data is valid
        for($intCpt = 0; ($intCpt < count($tabDataSrc)) && $result; $intCpt++)
        {
            $tabConfig = $tabDataSrc[$intCpt];

            $result =
                // Check valid config
                is_array($tabConfig) &&

                // Check valid permission data source
                (
                    (!isset($tabConfig[ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC])) ||
                    is_array($tabConfig[ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC])
                );
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified data source has valid format.
     *
     * @param mixed $dataSrc
     * @return boolean
     * @throws static
     */
    static public function setCheck($dataSrc)
    {
        // Init var
        $result =
            // Check valid array
            is_array($dataSrc) &&

            // Check valid data source
            static::checkDataSrcIsValid($dataSrc);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc));
        }

        // Return result
        return $result;
    }
	
	
	
}