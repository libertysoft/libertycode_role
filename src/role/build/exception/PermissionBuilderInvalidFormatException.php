<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\build\exception;

use liberty_code\role\permission\build\api\BuilderInterface;
use liberty_code\role\role\build\library\ConstBuilder;



class PermissionBuilderInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $builder
     */
	public function __construct($builder)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstBuilder::EXCEPT_MSG_PERMISSION_BUILDER_FORMAT,
            mb_strimwidth(strval($builder), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified array of builders has valid format.
     *
     * @param array $tabBuilder
     * @return boolean
     */
    protected static function checkTabBuilderIsValid(array $tabBuilder)
    {
        // Init var
        $result = true;
        $tabBuilder = array_values($tabBuilder);

        // Run all keys
        for($intCpt = 0; ($intCpt < count($tabBuilder)) && $result; $intCpt++)
        {
            // Get info
            $objBuilder = $tabBuilder[$intCpt];

            // Check key
            $result =
                // Check valid permission builder
                ($objBuilder instanceof BuilderInterface);
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified array of builders has valid format.
	 * 
     * @param mixed $builder
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($builder)
    {
		// Init var
		$result =
            // Check valid array
			(is_array($builder)) &&

            // Check valid builders
            static::checkTabBuilderIsValid($builder);
		
		// Throw exception if check not pass
		if(!$result)
		{
            throw new static((is_array($builder) ? serialize($builder) : $builder));
		}
		
		// Return result
		return $result;
    }
	
	
	
}