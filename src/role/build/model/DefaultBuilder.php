<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate role collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate role collection:
 * [
 *     // Role configuration 1
 *     Role factory configuration (@see RoleFactoryInterface ),
 *
 *     ...,
 *
 *     // Role configuration N
 *     ...
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\role\role\build\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\role\role\build\api\BuilderInterface;


use liberty_code\role\permission\build\api\BuilderInterface as PermissionBuilderInterface;
use liberty_code\role\permission\build\model\DefaultBuilder as PermissionBuilder;
use liberty_code\role\role\api\RoleCollectionInterface;
use liberty_code\role\role\factory\api\RoleFactoryInterface;
use liberty_code\role\role\build\library\ConstBuilder;
use liberty_code\role\role\build\exception\FactoryInvalidFormatException;
use liberty_code\role\role\build\exception\PermissionBuilderInvalidFormatException;
use liberty_code\role\role\build\exception\DataSrcInvalidFormatException;



/**
 * @method RoleFactoryInterface getObjFactory() Get role factory object.
 * @method void setObjFactory(RoleFactoryInterface $objFactory) Set role factory object.
 * @method PermissionBuilderInterface[] getTabPermissionBuilder() Get index array of permission builder objects.
 * @method void setTabPermissionBuilder(PermissionBuilderInterface[] $tabPermissionBuilder) Set index array of permission builder objects.
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends FixBean implements BuilderInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RoleFactoryInterface $objFactory
     * @param PermissionBuilderInterface[] $tabPermissionBuilder = array()
     * @param array $tabDataSrc = array()
     */
    public function __construct(
        RoleFactoryInterface $objFactory,
        array $tabPermissionBuilder = array(),
        array $tabDataSrc = array()
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init role factory
        $this->setObjFactory($objFactory);

        // Init permission builder array
        $this->setTabPermissionBuilder($tabPermissionBuilder);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_PERMISSION_BUILDER))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_PERMISSION_BUILDER] = array();
        }

        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }
    }



    /**
     * @inheritdoc
     */
    public function hydrateRoleCollection(RoleCollectionInterface $objRoleCollection, $boolClear = true)
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $objFactory = $this->getObjFactory();
        $tabPermissionBuilder = $this->getTabPermissionBuilder();
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabRole = array();
        foreach($tabDataSrc as $key => $tabConfig)
        {
            // Get permission data source, if required
            $tabPermissionDataSrc = array();
            if(
                array_key_exists(ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC, $tabConfig) &&
                is_array($tabConfig[ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC])
            )
            {
                $tabPermissionDataSrc = $tabConfig[ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC];
                unset($tabConfig[ConstBuilder::TAB_DATA_SRC_KEY_PERMISSION_DATA_SRC]);
            }

            // Get new role
            $key = (is_string($key) ? $key : null);
            $objRole = $objFactory->getObjRole($tabConfig, $key);

            // Register role, if found
            if(!is_null($objRole))
            {
                // Hydrate permission collection
                $objPermissionCollection = $objRole->getObjPermissionCollection();
                foreach($tabPermissionBuilder as $objPermissionBuilder)
                {
                    // Set data source array, if required
                    if($objPermissionBuilder instanceof PermissionBuilder)
                    {
                        $objPermissionBuilder->setTabDataSrc($tabPermissionDataSrc);
                    }

                    $objPermissionBuilder->hydratePermissionCollection($objPermissionCollection, false);
                }

                // Register role
                $tabRole[] = $objRole;
            }
            // Throw exception if role not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear roles from collection, if required
        if($boolClear)
        {
            $objRoleCollection->removeRoleAll();
        }

        // Register roles on collection
        $objRoleCollection->setTabRole($tabRole);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
            ConstBuilder::DATA_KEY_DEFAULT_FACTORY,
            ConstBuilder::DATA_KEY_DEFAULT_PERMISSION_BUILDER,
            ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
		$result = in_array($key, $tabKey);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
		// Init var
		$result = true;

		// Validation
		try
		{
			switch($key)
			{
                case ConstBuilder::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstBuilder::DATA_KEY_DEFAULT_PERMISSION_BUILDER:
                    PermissionBuilderInvalidFormatException::setCheck($value);
                    break;

                case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
                    break;
			}
		}
		catch(\Exception $e)
		{
			$result = false;
			$error = $e;
		}

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidRemove($key, &$error = null)
	{
		// Return result
		return false;
	}

	
	
}