<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/role/test/RoleTest.php');

// Use
use liberty_code\role\role\model\DefaultRoleCollection;



// Init var
$objRoleCollection = new DefaultRoleCollection();

$tabDataSrc = array(
    'role_1' => [
        'permission_data_src' => [
            'test_get' => [
                'enable' => false
            ],
            'test_set' => [
                'enable' => false
            ],
            'test_delete' => [
                'enable' => true
            ]
        ],
        'key-test-1' => 'Value test 1'
    ],
    'role_2' => [
        'permission_data_src' => [
            'test_get' => [
                'enable' => true
            ],
            'test_set' => [
                'enable' => true
            ],
            'test_add_1' => [
                'enable' => true
            ],
            'test_add_2' => [
                'enable' => false
            ]
        ],
        'key_test_1' => 'Value test 1',
        'key_test_2' => 'Value test 2'
    ],
    'role_3' => []
);
$objRoleBuilder->setTabDataSrc($tabDataSrc);



// Test hydrate role collection
echo('Test hydrate role collection: <br />');

echo('Before hydrate: <pre>');
foreach($objRoleCollection->getTabRoleName() as $strName)
{
    $objRole = $objRoleCollection->getObjRole($strName);
    echo(sprintf(
        'Role "%1$s": ',
        $objRole->getStrRoleName()
    ));
    echo('<br />');

    $objPermissionCollection = $objRole->getObjPermissionCollection();
    foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
    {
        $objPermission = $objPermissionCollection->getObjPermission($strKey);
        echo(sprintf(
            'Permission "%1$s": %2$s',
            $objPermission->getStrPermissionKey(),
            $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
        ));
        echo('<br />');
    }

    echo('<br />');
}
echo('</pre>');

$objRoleBuilder->hydrateRoleCollection($objRoleCollection);

echo('After hydrate: <pre>');
foreach($objRoleCollection->getTabRoleName() as $strName)
{
    $objRole = $objRoleCollection->getObjRole($strName);
    echo(sprintf(
        'Role "%1$s": ',
        $objRole->getStrRoleName()
    ));
    echo('<br />');

    $objPermissionCollection = $objRole->getObjPermissionCollection();
    foreach($objPermissionCollection->getTabPermissionKey() as $strKey)
    {
        $objPermission = $objPermissionCollection->getObjPermission($strKey);
        echo(sprintf(
            'Permission "%1$s": %2$s',
            $objPermission->getStrPermissionKey(),
            $objPermission->checkPermissionEnable() ? 'TRUE' : 'FALSE'
        ));
        echo('<br />');
    }

    echo('<br />');
}
echo('</pre>');

echo('<br /><br /><br />');



// Test get
echo('Test get role: <br />');

foreach($objRoleCollection->getTabRoleName() as $strRoleName)
{
    echo('Test get role name "'.$strRoleName.'": <br />');

    $objRole = $objRoleCollection->getObjRole($strRoleName);
    $objPermissionCollection = $objRole->getObjPermissionCollection();

    echo('Get role name: <pre>');var_dump($objRole->getStrRoleName());echo('</pre>');
    echo('Get role configuration: <pre>');var_dump($objRole->getTabRoleConfig());echo('</pre>');

    foreach($objRole->getObjPermissionCollection()->getTabPermissionKey() as $strKey)
    {
        echo('Check role, permission "'.$strKey.'" enable: <pre>');
        var_dump($objRole->checkPermissionEnable($strKey));
        echo('</pre>');

        echo('Check collection role, permission "'.$strKey.'" enable: <pre>');
        var_dump($objRoleCollection->checkPermissionEnable($strKey));
        echo('</pre>');
    }

    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


