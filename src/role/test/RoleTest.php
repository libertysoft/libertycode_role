<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/permission/test/PermissionTest.php');

// Use
use liberty_code\role\role\factory\standard\model\StandardRoleFactory;
use liberty_code\role\role\build\model\DefaultBuilder;



// Init factory
$objRoleFactory = new StandardRoleFactory();

// Init builder
$objRoleBuilder = new DefaultBuilder(
    $objRoleFactory,
    array(
        $objPermissionBuilder,
        $objPermissionSpecBuilder
    )
);


