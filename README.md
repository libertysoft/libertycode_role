LibertyCode_Role
================



Description
-----------

Library contains permission and role components, 
to manage access.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/role ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/role": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Subject

Subject allows to design an item, where permissions can be checked,
to access specific operations.

_Elements_

- Subject

    Allows to design a subject,
    where permissions enabled states can be checked.

#### Permission specification

Permission specification allows to specify specific set of permission keys,
in specific access system.

_Elements_

- PermSpec

    Allows to provide a scope of permission keys, 
    used in specific access system.
    
- StandardPermSpec

    Extends permission specification features. 
    Contains all information,
    to provide a scope of permission keys.

_Example_

```php
// Get permission specification
use liberty_code\role\permission\specification\standard\model\StandardPermSpec;
$permSpec = new StandardPermSpec(array(...));
...
// Get array of permission keys
$permSpec->getTabKey();
...
```

#### Permission

Permission is specific check-able right, to authorize specific operations,
in specific access system.

_Elements_

- Permission

    Allows to design a permission,
    to authorize specific operations.

- PermissionCollection

    Extends subject features. 
    Allows to design collection of permissions.
    Allows to check if specified permission is enabled.

- PermissionFactory

    Allows to design permission factory, 
    to provide new or specified permission instances,
    from specified configuration.
    
- StandardPermissionFactory

    Extends permission factory features. 
    Provides permission instance.

- Builder
    
    Builder allows to hydrate permission collection, 
    with permissions.
    
- PermSpecBuilder
    
    Extends builder features. 
    Uses permission specification instance to hydrate permission collection.
    
- PermissionSubject

    Extends subject features. 
    Allows to design a permission subject,
    where permissions can be attributed.
    
```php
// Get permission factory
use liberty_code\role\permission\factory\standard\model\StandardPermissionFactory;
$permissionFactory = new StandardPermissionFactory();
...
// Get permission builder
use liberty_code\role\permission\build\specification\model\PermSpecBuilder;
$permissionBuilder = new PermSpecBuilder($permSpec, $permissionFactory);
...
// Get permission collection
use liberty_code\role\permission\model\DefaultPermissionCollection;
$permissionCollection = new DefaultPermissionCollection();
...
// Hydrate permission collection
$permissionBuilder->hydratePermissionCollection($permissionCollection);
...
foreach($permissionCollection->getTabPermissionKey() as $permissionKey) {
    echo($permissionCollection->getObjPermission($permissionKey)->getStrPermissionKey() .'<br />');
}
/**
 * Show: 
 * Permission key 1
 * ...
 * Permission key N
 */
...
```

#### Permission subject

Permission subject allows to design subject, where permissions can be attributed,
to access specific operations.

_Elements_

- PermissionSubject

    Extends subject features. 
    Allows to design a permission subject,
    where permissions can be attributed.
    
```php
// Get subject
use liberty_code\role\permission\subject\model\DefaultPermissionSubject;
$subject = new DefaultPermissionSubject(
    $permissionCollection
);
...
// Check specific permission enabled, for subject
$subject->checkPermissionEnable(...'string permission key');
...
```

#### Role

Role allows to design named group of permissions.

_Elements_

- Role

    Extends permission subject features. 
    Allows to design a role,
    which is a named group of permissions, 
    to check if specified permission is enabled.

- RoleCollection
    
    Extends subject features. 
    Allows to design collection of roles.
    Allows to check if specified permission is enabled 
    on at least one of specific roles.

- RoleFactory

    Allows to design role factory, 
    to provide new or specified role instances,
    from specified configuration.
    
- StandardRoleFactory

    Extends role factory features. 
    Provides role instance.

- Builder
    
    Builder allows to hydrate role collection, 
    with roles.
    
```php
// Get role factory
use liberty_code\role\role\factory\standard\model\StandardRoleFactory;
$roleFactory = new StandardRoleFactory();
...
// Get role builder
use liberty_code\role\role\build\model\DefaultBuilder;
$roleBuilder = new DefaultBuilder(
    $roleFactory, 
    array(
        $permissionBuilder
    )
);
...
// Get role collection
use liberty_code\role\role\model\DefaultRoleCollection;
$roleCollection = new DefaultRoleCollection();
...
// Hydrate role collection
$roleBuilder->hydrateRoleCollection($roleCollection);
...
foreach($roleCollection->getTabRoleName() as $roleName) {
    echo($roleCollection->getObjRole($roleName)->getStrRoleName() .'<br />');
}
/**
 * Show: 
 * Role name 1
 * ...
 * Role name N
 */
...
```

#### Role subject

Role subject allows to design subject, where roles can be attributed,
to access specific operations.

_Elements_

- RoleSubject

    Extends subject features. 
    Allows to design a role subject,
    where roles can be attributed.
    
- RolePermissionSubject

    Extends permission subject features. 
    Allows to design a role permission subject,
    where roles can be attributed.
    
```php
// Get subject
use liberty_code\role\role\subject\model\DefaultRolePermissionSubject;
$subject = new DefaultRolePermissionSubject(
    $permissionCollection,
    $roleCollection
);
...
// Check specific permission enabled, for subject
$subject->checkPermissionEnable(...'string permission key');
...
// Check specific role exists, for subject
$subject->checkRoleExists(...'string role name');
...
```

---


