<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/subject/api/SubjectInterface.php');
include($strRootPath . '/src/subject/library/ToolBoxSubject.php');

include($strRootPath . '/src/permission/specification/library/ConstPermSpec.php');
include($strRootPath . '/src/permission/specification/library/ToolBoxPermSpec.php');
include($strRootPath . '/src/permission/specification/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/permission/specification/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/permission/specification/api/PermSpecInterface.php');
include($strRootPath . '/src/permission/specification/model/DefaultPermSpec.php');

include($strRootPath . '/src/permission/specification/standard/library/ConstStandardPermSpec.php');
include($strRootPath . '/src/permission/specification/standard/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/permission/specification/standard/model/StandardPermSpec.php');

include($strRootPath . '/src/permission/library/ConstPermission.php');
include($strRootPath . '/src/permission/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/permission/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/permission/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/permission/api/PermissionInterface.php');
include($strRootPath . '/src/permission/api/PermissionCollectionInterface.php');
include($strRootPath . '/src/permission/model/DefaultPermission.php');
include($strRootPath . '/src/permission/model/DefaultPermissionCollection.php');

include($strRootPath . '/src/permission/factory/library/ConstPermissionFactory.php');
include($strRootPath . '/src/permission/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/permission/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/permission/factory/api/PermissionFactoryInterface.php');
include($strRootPath . '/src/permission/factory/model/DefaultPermissionFactory.php');

include($strRootPath . '/src/permission/factory/standard/library/ConstStandardPermissionFactory.php');
include($strRootPath . '/src/permission/factory/standard/model/StandardPermissionFactory.php');

include($strRootPath . '/src/permission/build/library/ConstBuilder.php');
include($strRootPath . '/src/permission/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/permission/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/permission/build/api/BuilderInterface.php');
include($strRootPath . '/src/permission/build/model/DefaultBuilder.php');

include($strRootPath . '/src/permission/build/specification/library/ConstPermSpecBuilder.php');
include($strRootPath . '/src/permission/build/specification/exception/PermSpecInvalidFormatException.php');
include($strRootPath . '/src/permission/build/specification/model/PermSpecBuilder.php');

include($strRootPath . '/src/permission/subject/library/ConstPermissionSubject.php');
include($strRootPath . '/src/permission/subject/exception/PermissionCollectionInvalidFormatException.php');
include($strRootPath . '/src/permission/subject/api/PermissionSubjectInterface.php');
include($strRootPath . '/src/permission/subject/model/DefaultPermissionSubject.php');

include($strRootPath . '/src/permission/subject/build/library/ToolBoxPermissionBuilder.php');

include($strRootPath . '/src/role/library/ConstRole.php');
include($strRootPath . '/src/role/exception/PermissionCollectionInvalidFormatException.php');
include($strRootPath . '/src/role/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/role/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/role/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/role/api/RoleInterface.php');
include($strRootPath . '/src/role/api/RoleCollectionInterface.php');
include($strRootPath . '/src/role/model/DefaultRole.php');
include($strRootPath . '/src/role/model/DefaultRoleCollection.php');

include($strRootPath . '/src/role/factory/library/ConstRoleFactory.php');
include($strRootPath . '/src/role/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/role/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/role/factory/api/RoleFactoryInterface.php');
include($strRootPath . '/src/role/factory/model/DefaultRoleFactory.php');

include($strRootPath . '/src/role/factory/standard/library/ConstStandardRoleFactory.php');
include($strRootPath . '/src/role/factory/standard/model/StandardRoleFactory.php');

include($strRootPath . '/src/role/build/library/ConstBuilder.php');
include($strRootPath . '/src/role/build/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/role/build/exception/PermissionBuilderInvalidFormatException.php');
include($strRootPath . '/src/role/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/role/build/api/BuilderInterface.php');
include($strRootPath . '/src/role/build/model/DefaultBuilder.php');

include($strRootPath . '/src/role/subject/library/ConstRoleSubject.php');
include($strRootPath . '/src/role/subject/exception/RoleCollectionInvalidFormatException.php');
include($strRootPath . '/src/role/subject/api/RoleSubjectInterface.php');
include($strRootPath . '/src/role/subject/model/DefaultRoleSubject.php');
include($strRootPath . '/src/role/subject/model/DefaultRolePermissionSubject.php');

include($strRootPath . '/src/role/subject/build/permission/library/ToolBoxPermissionBuilder.php');
include($strRootPath . '/src/role/subject/build/library/ToolBoxRoleBuilder.php');